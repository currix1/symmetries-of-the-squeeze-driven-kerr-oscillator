# Symmetries of the squeeze-driven Kerr oscillator

Manuscript submitted to *Journal of Physics A*

Authors: Francesco Iachello, Rodrigo G. Cortiñas, Francisco Pérez Bernal, and Lea F. Santos.

## Manuscript data files

- Figure 1:
  (x,y) data for the two possible symmetries.
  
  Files `figure_1_even.dat`, `figure_1_odd.dat`
  
- Figure 2:
  (x,y) data for the two possible symmetries.
  
  Files `figure_2_minus.dat`, `figure_2_plus.dat`
  
- Figure 3:
  (x,y) data for the two possible symmetries.
  
  Files `figure_3_minus.dat`, `figure_3_plus.dat`
  
- Figure 4:
  (x, y1, y2, ...) data for the two possible symmetries.

  Files `figure_4_even.dat`, `figure_4_odd.dat`
  
- Figure 5:
  (x, y1, y2, ...) data for the two panels.

  Files `figure_5_a.dat`, `figure_5_b.dat`
  

- Figure 6:
  Panel a: `Fig6a_Eo_EeLess0_005N500.dat`, `Fig6a_LinearExtrapN500.dat`, `Fig6a_MaxRateApprN500.dat`
  
  Panel b: `Fig6b_Eo_EeLess0_005N500.dat`, `Fig6b_LinearExtrapN500.dat`, `Fig6b_MaxRateApprN500.dat`

- Figure 7:
  (x, y1, y2, ...) data

  Files: `figure_7_even.dat`. `figure_7_odd.dat`
  
- Figure 8:
  Panel a,  (x, y1, y2, ...) data 

  Files: `figure_8a_xi0_even.dat`, `figure_8a_xi0_odd.dat`, `figure_8a_xi1_even.dat`, `figure_8a_xi1_odd.dat`, `figure_8a_xi2_even.dat`, `figure_8a_xi2_odd.dat`, `figure_8a_xi3_even.dat`, `figure_8a_xi3_odd.dat`,

  Panel b,  (x, y1, y2, ...) data 

  Files:  `figure_8b_xi0_even.dat`, `figure_8b_xi0_odd.dat`, `figure_8b_xi1_even.dat`, `figure_8b_xi1_odd.dat`, `figure_8b_xi2_even.dat`, `figure_8b_xi2_odd.dat`, `figure_8b_xi3_even.dat`, `figure_8b_xi3_odd.dat`

- Figure 9:
  eta = 5,  (x, y1, y2, ...) data 
  Files: `figure_9_eta5_even.dat`, `figure_9_eta5_odd.dat`
  
  eta = 6,  (x, y1, y2, ...) data 
  Files: `figure_9_eta6_even.dat`, `figure_9_eta6_odd.dat`

- Figure 10:
  Panel a: even and odd symmetry (x, y1, y2, ...)  data. 
  Files: `figure_10a_even.dat`, `figure_10a_odd.dat`

  Panel b: mod 4 symmetries  (x, y1, y2, ...)  data. 
  Files: `figure_10b_s0.dat`, `figure_10b_s1.dat`, `figure_10b_s2.dat`, `figure_10b_s3.dat`

- Figure 11:
  Panels a, even and odd symmetry (x, y1, y2, ...)  data.

  Files: `figure_11a_xi0_even.dat`, `figure_11a_xi0_odd.dat`, `figure_11a_xi1_even.dat`, `figure_11a_xi1_odd.dat`, `figure_11a_xi2_even.dat`, `figure_11a_xi2_odd.dat`, `figure_11a_xi3_even.dat`, `figure_11a_xi3_odd.dat`

  Panels b, mod 3 symmetry (x, y1, y2, ...)  data.

  Files: `figure_11b_xi0_s0.dat`, `figure_11b_xi0_s1.dat`, `figure_11b_xi0_s2.dat`, `figure_11b_xi0_s3.dat`, `figure_11b_xi1_s0.dat`, `figure_11b_xi1_s1.dat`, `figure_11b_xi1_s2.dat`, `figure_11b_xi1_s3.dat`, `figure_11b_xi2_s0.dat`, `figure_11b_xi2_s1.dat`, `figure_11b_xi2_s2.dat`, `figure_11b_xi2_s3.dat`, `figure_11b_xi3_s0.dat`, `figure_11b_xi3_s1.dat`, `figure_11b_xi3_s2.dat`, `figure_11b_xi3_s3.dat`

- Figure 12:
  (x, y1, y2, ...) data with mod 3 symmetry
  
  Files: `figure_12_s0.dat`, `figure_12_s1.dat`, `figure_12_s2.dat`

- Figure 13:
  (x, y1, y2, ...) data with mod 3 symmetry
  
  Files: `figure_13_xi0_s0.dat`, `figure_13_xi0_s1.dat`, `figure_13_xi0_s2.dat`, `figure_13_xi1_s0.dat`, `figure_13_xi1_s1.dat`, `figure_13_xi1_s2.dat`, `figure_13_xi2_s0.dat`, `figure_13_xi2_s1.dat`, `figure_13_xi2_s2.dat`, `figure_13_xi3_s0.dat`, `figure_13_xi3_s1.dat`, `figure_13_xi3_s2.dat`

- Figure 14:
  (x, y1, y2, ...) data for P3 and P4 operators

  Files: `figure_14_P3.dat`, `figure_14_P4.dat`



